$(document).ready(function() {
    document.getElementById('bars').addEventListener('click', function(){
        var links = document.getElementById('links');
        if (links.style.display === 'none'){
            document.getElementById('banner-text').style.display = "none";
            links.style.display = 'initial';
        } else {
            document.getElementById('banner-text').style.display = "block";
            links.style.display = 'none';
        }
    });
});
document.getElementById('dot1').addEventListener("click", function(){
    document.getElementById("tweet2").classList.remove("show");
    document.getElementById("tweet3").classList.remove("show");
    document.getElementById("tweet2").classList.add("hide");
    document.getElementById("tweet3").classList.add("hide");
    document.getElementById("tweet1").classList.remove("hide");
    document.getElementById("tweet1").classList.add("show");
});

document.getElementById('dot2').addEventListener("click", function(){
    document.getElementById("tweet1").classList.remove("show");
    document.getElementById("tweet3").classList.remove("show");
    document.getElementById("tweet1").classList.add("hide");
    document.getElementById("tweet3").classList.add("hide");
    document.getElementById("tweet2").classList.remove("hide");
    document.getElementById("tweet2").classList.add("show");
});

document.getElementById('dot3').addEventListener("click", function(){
    document.getElementById("tweet1").classList.remove("show");
    document.getElementById("tweet2").classList.remove("show");
    document.getElementById("tweet1").classList.add("hide");
    document.getElementById("tweet2").classList.add("hide");
    document.getElementById("tweet3").classList.remove("hide");
    document.getElementById("tweet3").classList.add("show");
});


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyIsInR3aXR0ZXJfY2Fyb3VzZWwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoic2NyaXB0cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2JhcnMnKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyIGxpbmtzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2xpbmtzJyk7XHJcbiAgICAgICAgaWYgKGxpbmtzLnN0eWxlLmRpc3BsYXkgPT09ICdub25lJyl7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdiYW5uZXItdGV4dCcpLnN0eWxlLmRpc3BsYXkgPSBcIm5vbmVcIjtcclxuICAgICAgICAgICAgbGlua3Muc3R5bGUuZGlzcGxheSA9ICdpbml0aWFsJztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYmFubmVyLXRleHQnKS5zdHlsZS5kaXNwbGF5ID0gXCJibG9ja1wiO1xyXG4gICAgICAgICAgICBsaW5rcy5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KTsiLCJkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZG90MScpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbigpe1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDJcIikuY2xhc3NMaXN0LnJlbW92ZShcInNob3dcIik7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0M1wiKS5jbGFzc0xpc3QucmVtb3ZlKFwic2hvd1wiKTtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQyXCIpLmNsYXNzTGlzdC5hZGQoXCJoaWRlXCIpO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDNcIikuY2xhc3NMaXN0LmFkZChcImhpZGVcIik7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0MVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwiaGlkZVwiKTtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQxXCIpLmNsYXNzTGlzdC5hZGQoXCJzaG93XCIpO1xyXG59KTtcclxuXHJcbmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdkb3QyJykuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uKCl7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0MVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwic2hvd1wiKTtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQzXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJzaG93XCIpO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDFcIikuY2xhc3NMaXN0LmFkZChcImhpZGVcIik7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0M1wiKS5jbGFzc0xpc3QuYWRkKFwiaGlkZVwiKTtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJoaWRlXCIpO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDJcIikuY2xhc3NMaXN0LmFkZChcInNob3dcIik7XHJcbn0pO1xyXG5cclxuZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2RvdDMnKS5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24oKXtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQxXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJzaG93XCIpO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDJcIikuY2xhc3NMaXN0LnJlbW92ZShcInNob3dcIik7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0MVwiKS5jbGFzc0xpc3QuYWRkKFwiaGlkZVwiKTtcclxuICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHdlZXQyXCIpLmNsYXNzTGlzdC5hZGQoXCJoaWRlXCIpO1xyXG4gICAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0d2VldDNcIikuY2xhc3NMaXN0LnJlbW92ZShcImhpZGVcIik7XHJcbiAgICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInR3ZWV0M1wiKS5jbGFzc0xpc3QuYWRkKFwic2hvd1wiKTtcclxufSk7XHJcblxyXG4iXX0=
