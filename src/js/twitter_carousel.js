document.getElementById('dot1').addEventListener("click", function(){
    document.getElementById("tweet2").classList.remove("show");
    document.getElementById("tweet3").classList.remove("show");
    document.getElementById("tweet2").classList.add("hide");
    document.getElementById("tweet3").classList.add("hide");
    document.getElementById("tweet1").classList.remove("hide");
    document.getElementById("tweet1").classList.add("show");
});

document.getElementById('dot2').addEventListener("click", function(){
    document.getElementById("tweet1").classList.remove("show");
    document.getElementById("tweet3").classList.remove("show");
    document.getElementById("tweet1").classList.add("hide");
    document.getElementById("tweet3").classList.add("hide");
    document.getElementById("tweet2").classList.remove("hide");
    document.getElementById("tweet2").classList.add("show");
});

document.getElementById('dot3').addEventListener("click", function(){
    document.getElementById("tweet1").classList.remove("show");
    document.getElementById("tweet2").classList.remove("show");
    document.getElementById("tweet1").classList.add("hide");
    document.getElementById("tweet2").classList.add("hide");
    document.getElementById("tweet3").classList.remove("hide");
    document.getElementById("tweet3").classList.add("show");
});

