$(document).ready(function() {
    document.getElementById('bars').addEventListener('click', function(){
        var links = document.getElementById('links');
        if (links.style.display === 'none'){
            document.getElementById('banner-text').style.display = "none";
            links.style.display = 'initial';
        } else {
            document.getElementById('banner-text').style.display = "block";
            links.style.display = 'none';
        }
    });
});